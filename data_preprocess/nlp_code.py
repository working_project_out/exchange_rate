import tensorflow as tf 
from tensorflow import keras
from tensorflow.keras.layers import Attention

from sklearn.model_selection import train_test_split
import unicodedata
import re
import numpy as np
import os
import io
import time
import pandas as pd

def preprocess_sentence(w):
    # w = unicode_to_ascii(w.lower().strip())

    # creating a space between a word and the punctuation following it
    # eg: "he is a boy." => "he is a boy ."
    # Reference:- https://stackoverflow.com/questions/3645931/python-padding-punctuation-with-white-spaces-keeping-punctuation
    w = re.sub(r"([?.!,¿])", r" \1 ", w)
    w = re.sub(r'[" "]+', " ", w)

    # replacing everything with space except (a-z, A-Z, ".", "?", "!", ",")
    w = re.sub(r"[^a-zA-Z?.!,¿]+", " ", w)

    w = w.rstrip().strip()

    # adding a start and an end token to the sentence
    # so that the model know when to start and stop predicting.
    w = '<start> ' + w + ' <end>'
    return w

def max_length(tensor):
    return max(len(t) for t in tensor)


def tokenize(lang):
    lang_tokenizer = tf.keras.preprocessing.text.Tokenizer(
      filters='', num_words=12000)
    lang_tokenizer.fit_on_texts(lang)

    tensor = lang_tokenizer.texts_to_sequences(lang)

    tensor = tf.keras.preprocessing.sequence.pad_sequences(tensor,
                                                         padding='post')

    return tensor, lang_tokenizer

def convert(lang, tensor):
  for t in tensor:
    if t!=0:
      print ("%d ----> %s" % (t, lang.index_word[t]))


path = '../../data/news_data/reuters/reuters-newswire-2007.v5.csv'
data = pd.read_csv(path)
tex = data['headline_text']
time_pub = data['publish_time']
tex_out = []
time_pub_out = []
for x, y in zip(tex, time_pub):
    if type(x) is str:
        tex_out.append(preprocess_sentence(x))
        time_pub_out.append(y)
tex = tex_out
time_pub = time_pub_out

news_tensor, token_news = tokenize(tex)
max_len = max_length(news_tensor)

convert(token_news, news_tensor[0])

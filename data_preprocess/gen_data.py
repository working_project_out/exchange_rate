import tensorflow as tf 
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.layers import Attention, Flatten

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import unicodedata
import re
import numpy as np
import os
import io
import time
import pandas as pd
import joblib

def tokenize(lang):
    lang_tokenizer = tf.keras.preprocessing.text.Tokenizer(
      filters='', num_words=12000)
    lang_tokenizer.fit_on_texts(lang)

    tensor = lang_tokenizer.texts_to_sequences(lang)

    tensor = tf.keras.preprocessing.sequence.pad_sequences(tensor,
                                                         padding='post')

    return tensor, lang_tokenizer

def convert(lang, tensor):
  for t in tensor:
    if t!=0:
      print ("%d ----> %s" % (t, lang.index_word[t]))

def exchage_data_process(data):
    cld = data['Local time']
    # date = np.zeros(len(cld), 4)
    date_record = []
    week_day = 0
    for i, x  in enumerate(cld):
        temp = []
        temp.append(x[:2])
        temp.append(x[3:5])
        temp.append(x[6:10])
        temp.append(str(week_day))
        date_record.append(temp)
        week_day+=1
        if week_day>=5:
            week_day=0
        # print(i)

    date_t, token_date = tokenize(date_record)
    # print(date_t)
    # convert(token_date, date_t[0])
    price_data = data.iloc[:,1:]
    price_data = np.array(price_data)
    price_data[:,4] = price_data[:,4]/(10e8)
    # print(price_data)
    return date_t, price_data

gbp_usd = pd.read_csv('/home/quang/work/data/foregin_exchange/GBPUSD_Candlestick_1_D_BID_01.01.2007-31.08.2019.csv')
# eur_gbp = pd.read_csv('/home/quang/work/data/foregin_exchange/EURGBP_Candlestick_1_D_BID_01.01.2007-31.08.2019.csv')
# eur_usd = pd.read_csv('/home/quang/work/data/foregin_exchange/EURUSD_Candlestick_1_D_BID_01.01.2007-31.08.2019.csv')
# usd_cad = pd.read_csv('/home/quang/work/data/foregin_exchange/USDCAD_Candlestick_1_D_BID_01.01.2007-31.08.2019.csv')
# usd_jpy = pd.read_csv('/home/quang/work/data/foregin_exchange/USDJPY_Candlestick_1_D_BID_01.01.2007-31.08.2019.csv')

date_record, price_data = exchage_data_process(gbp_usd)
print(price_data.shape, date_record.shape)

scaler = StandardScaler()
scaler.fit(price_data)
price_data = scaler.transform(price_data)
# joblib.dump(scaler, 'scaler_gbp_usd')


def data_generator(data, batch_size):
    date_in, price_data = data
    # date_embedding = layers.Embedding(100, 4, input_length=4)
    while True:
        for i in range(30, len(price_data)-10):
            price_input = price_data[i-30:i]
            date_imput = date_in[i-30:i]

            price_out = price_data[]






